#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/firebase-test-lab-pipe"}
  cp test/*.apk .

#  echo "Building image..."
#  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
 #   run docker run --rm \
 #         -e NAME="baz" \
 #         -v $(pwd):$(pwd) \
 #         -w $(pwd) \
 #         -e PROJECT_ID="driving-dao" \
 #         -e GOOGLE_CLOUD_KEY="${GOOGLE_CLOUD_KEY}" \
 #         -e TYPE="instrumentation" \
 #         -e APP="app-debug.apk" \
 #         -e TEST="app-debug-androidTest.apk" \
 #         -e DEVICE="model=flame,version=29,locale=pt_BR,orientation=portrait model=flame,version=19,locale=pt_BR,orientation=portrait" \
 #         -e DEBUG="true" \
 #       ${DOCKER_IMAGE}:test

 run docker build -t ${DOCKER_IMAGE}:test .

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

