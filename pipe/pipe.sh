#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"

enable_debug

info "iniciando Firebase Test Lab ..."

# PARAMETROS OBRIGATORIOS
PROJECTID=${PROJECT_ID:?'PROJECT_ID não informado.'}
GOOGLECLOUDKEY=${GOOGLE_CLOUD_KEY:?'GOOGLE_CLOUD_KEY não informado.'}
debug PROJECT-ID: "${PROJECT_ID}"
debug GOOGLE-CLOUD-KEY: "${GOOGLE_CLOUD_KEY}"

DIR=${FLAVOR:+"/${FLAVOR}"}
FLAVOR=${FLAVOR:+"-${FLAVOR}"}

APP=${APP:="app/build/outputs/apk${DIR}/debug/app${FLAVOR}-debug.apk"}
TEST=${TEST:="app/build/outputs/apk/androidTest${DIR}/debug/app${FLAVOR}-debug-androidTest.apk"}
debug APK-APP: "${APP}"
debug APK-TEST: "${TEST}"

# PARAMETROS OPCIONAIS
TYPE=${TYPE:="instrumentation"}
debug TEST-TYPE: "${TYPE}"

echo "${GOOGLE_CLOUD_KEY}" | base64 --decode >> /tmp/credentials.json
run gcloud auth activate-service-account --key-file /tmp/credentials.json --quiet 
run gcloud config set project "${PROJECT_ID}" --quiet 

ARGS="--type ${TYPE}"

if [ -f "${APP}" ]; then
  ARGS="${ARGS} --app ${APP}"
else 
  fail "apk da aplicação não localizado!"
fi

if [ -f "$TEST" ]; then
  ARGS="${ARGS} --test ${TEST}"
else 
  fail "apk de teste não localizado!"
fi

ARGS="${ARGS} --device $(echo ${DEVICE} | sed 's/ / --device /g')" 
debug ARGS: "${ARGS}"

info "iniciando execução dos testes ..."
run gcloud firebase test android run $ARGS

if [[ "${status}" == "0" ]]; then
  success "Firebase Test Lab executado com sucesso!"
else
  fail "Firebase Test Lab falhou! :("
fi