# Bitbucket Pipelines Pipe: Firebase Test Lab

Este pipe executa teste no Firebase Test Lab

## Definição de YAML

Adicione o seguinte trecho à seção de script do seu arquivo `bitbucket-pipelines.yml`:

```yaml
script:
  - pipe: diogo0liveira/firebase-test-lab-pipe:1.0.0
    variables:
      PROJECT_ID: "<string>"
      GOOGLE_CLOUD_KEY: "<string>"
      #APP: "<string>" # Optional
      #TEST: "<string>" # Optional
      #FLAVOR: "<string>" # Optional
      # TYPE: "<string>" # Optional
      # DEVICE: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variáveis

| Variável Uso         | Descrição                                                                                                          |
| -------------------- | ------------------------------------------------------------------------------------------------------------------ |
| PROJECT_ID (*)       | Id do projeto. Pode ser localizado no console do Firebase                                                          |
| GOOGLE_CLOUD_KEY (*) | Json key de autorização do Google Cloud codificado em base64                                                       |
| APP                  | APK do aplicativo. Padrão:(`app/build/outputs/apk/debug/app-debug.apk`)                                            |
| TEST                 | APK de teste. Padrão:(`app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk`)                         |
| FLAVOR               | Flavor que será utilizado. Quando informado, tem prioridade sobre APP e TEST respectivamente e o diretório padrão é:(`app/build/outputs/apk/{flavor}/debug/app-{flavor}-debug.apk` e `app/build/outputs/apk/androidTest/{flavor}/debug/app/{flavor}-debug-androidTest.apk`).                                                                                                         |
| DEVICE               | Informações sobre o device: <https://firebase.google.com/docs/test-lab/android/command-line?hl=pt-br#choosing_test_configurations>                                                                                                      |
| DEBUG                | Ativar informações extras de depuração. Padrão: `false`                                                            |

_ (*) ​​= variável necessária._

## Pré-requisitos

## Exemplos

Exemplo básico:

```yaml
- step:
  name: INSTR. TESTS
  script:
    - pipe: docker://diogo0liveira/firebase-test-lab-pipe:1.0.0
      variables:
        PROJECT_ID: $PROJECT_ID
        GOOGLE_CLOUD_KEY: $GOOGLE_CLOUD_KEY
             
        TYPE: 'instrumentation'
        APP: 'app/build/outputs/apk/debug/app-debug.apk'
        TEST: 'app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk'
        DEVICE: 'model=flame,version=29,locale=pt_BR,orientation=portrait 
```

## Apoio, suporte
Se você precisar de ajuda com este canal ou tiver um problema ou solicitação de recurso, informe-nos.
O tubo é mantido por diogo0liveira@hotmail.com.

Se você estiver relatando um problema, inclua:

- a versão do pipe
- logs e mensagens de erro relevantes
- Passos para reproduzir